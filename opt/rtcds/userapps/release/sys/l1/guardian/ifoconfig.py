##################################################
# aLIGO IFO PARAMETERS FILE
##################################################

##################################################
# INPUT

# threshold
imc_refl_no_light_threshold = 20

##################################################
# FULL LOCK

# 2000 is for 1W input.  It's scaled where it's used in the code
drmi_locked_threshold_pop18i = 2000  

carm_offset_diff_servo   = -8.0
carm_offset_qpds_engage  = -12.0
engage_slow_wfs_wait     = 3
carm_offset_asq          = -15
carm_offset_refl9        = -150 #-120

diff_beatnote_threshold = -12

#Calibration lines
calibration_line1_freq = 34.9
calibration_line1_clkgain = 0.0112
calibration_line2_freq = 535.1
calibration_line2_clkgain = 0.9957

etmy_lkin_p_freq = 35.7
etmy_lkin_p_clkgain = 0.0117
etmy_lkin_y_freq = 538.7
etmy_lkin_y_clkgain = 0.9703


#Pcal lines
pcalx_line1_freq = 33.7
pcalx_line1_amp = 20.0
pcaly_line1_freq = 37.3
pcaly_line1_amp = 25.0
pcalx_line2_freq = 533.3
pcalx_line2_amp = 3797.9
pcaly_line2_freq = 539.9
pcaly_line2_amp = 3500


###################################################
# Single Arm Lock

arm_locked_threshold = 15
